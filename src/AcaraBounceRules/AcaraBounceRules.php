<?php

namespace AcaraBounceRules;

class Rules {

    private $data;

    public function __construct()
    {
        $file = realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'rules.json';
        $this->data = json_decode(file_get_contents($file));
    }

    public function getRules()
    {
        if ($this->data) {
            return $this->data->rules;
        }
        return false;
    }

    public function getTypes()
    {
        if ($this->data) {
            $result = array();
            $types = $this->data->types;
            foreach ($types as $type) {
                $result[$type->type] = $type->result;
            }
            return $result;
        }
        return false;
    }

    public function getData()
    {
        if ($this->data) {
            return $this->data;
        }
        return false;
    }

}
