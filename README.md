Acara Bounce Rules provides a simple way to maintain bounce conditions for email handling.

## License

Acara Bounce Rules is licensed under the BSD License. See the LICENSE file for details.